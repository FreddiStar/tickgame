package de.almostintelligent.tickgame;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class World {

    private static double humidity = 54.55181;
    private static double elevation = 141.958032842;

    public static class Data {
        public int h;
        public int e;
    }

    public static void main(String[] args) {

        ArrayList<ArrayList<Data>> data = new ArrayList<ArrayList<Data>>();

        int max = 100;
        int humiditySteps = 6;
        int elevationSteps = 4;

        for (int x = 0; x < max; ++x) {

            ArrayList<Data> row = new ArrayList<Data>();

            for (int y = 0; y < max; ++y) {
                Data d = new Data();

                d.h = (int) Math.floor(humiditySteps * 0.5 * (1 + ImprovedNoise.noise(rel(max, x), rel(max, y), humidity)));
                d.e = (int) Math.floor(elevationSteps * 0.5 * (1 + ImprovedNoise.noise(rel(max, x), rel(max, y), elevation)));

                row.add(d);
            }

            data.add(row);
        }

        GsonBuilder builder = new GsonBuilder();
        System.out.print(builder.create().toJson(data));


    }

    private static float rel(int max, int x) {
        return (x * 1.0f) / (max * 1.0f);
    }
}
