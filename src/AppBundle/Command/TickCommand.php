<?php

namespace AppBundle\Command;

use AppBundle\Model\ConstructibleModel;
use DataBundle\Entity\Constructible;
use DataBundle\Entity\ConstructibleProduction;
use DataBundle\Entity\Construction;
use DataBundle\Entity\PlayerHasResearch;
use DataBundle\Entity\Resource;
use DataBundle\Entity\VillageHasBuilding;
use DataBundle\Entity\VillageHasResource;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TickCommand extends ContainerAwareCommand
{
    /** @var EntityManager $em */
    private $em;

    protected function configure()
    {
        $this->setName("game:tick");
        $this->setDescription("Do a game Tick");


    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();

        $output->writeln("GameTick");

        $this->doConstructions($output);
        $this->doResources($output);

    }

    protected function doConstructions(OutputInterface $output)
    {
        $output->writeln("Constructions Begin");
        $constructions = $this->em->getRepository("DataBundle:Construction")->findAll();
        $villageHasBuildingRepo = $this->em->getRepository("DataBundle:VillageHasBuilding");
        $playerHasResearchRepo = $this->em->getRepository("DataBundle:PlayerHasResearch");

        /** @var Construction $construction */
        foreach ($constructions as $construction) {
            if ($construction->getTicksLeft() == 1) {
                if ($construction->getConstructible()->getType() == 'building') {
                    /** @var VillageHasBuilding $villageHasBuilding */
                    $villageHasBuilding = $villageHasBuildingRepo->findOneBy(
                        array(
                            'village' => $construction->getVillage(),
                            'constructible' => $construction->getConstructible()
                        ));

                    if ($villageHasBuilding) {
                        $output->writeln("Updated " . $villageHasBuilding->getConstructible()->getName() . " to " . ($villageHasBuilding->getLevel() + 1));
                        $villageHasBuilding->setLevel($villageHasBuilding->getLevel() + 1);
                    } else {
                        $output->writeln("Building " . $construction->getConstructible()->getName() . " in " . $construction->getVillage()->getName());
                        $villageHasBuilding = new  VillageHasBuilding();
                        $villageHasBuilding->setConstructible($construction->getConstructible());
                        $villageHasBuilding->setVillage($construction->getVillage());
                        $villageHasBuilding->setLevel(1);
                    }

                    $this->em->persist($villageHasBuilding);
                    $this->em->remove($construction);
                    $this->em->flush();
                } else if ($construction->getConstructible()->getType() == 'research') {
                    /** @var PlayerHasResearch $playerHasResearch */
                    $playerHasResearch = $playerHasResearchRepo->findOneBy(
                        array(
                            'player' => $construction->getPlayer(),
                            'constructible' => $construction->getConstructible()
                        ));
                    if ($playerHasResearch) {
                        $output->writeln("Updated " . $playerHasResearch->getConstructible()->getName() . " to " . ($playerHasResearch->getLevel() + 1));
                        $playerHasResearch->setLevel($playerHasResearch->getLevel() + 1);
                    } else {
                        $output->writeln("Researching " . $construction->getConstructible()->getName() . " for " . $construction->getPlayer()->getName());
                        $playerHasResearch = new PlayerHasResearch();
                        $playerHasResearch->setLevel(1);
                        $playerHasResearch->setConstructible($construction->getConstructible());
                        $playerHasResearch->setPlayer($construction->getPlayer());
                    }

                    $this->em->persist($playerHasResearch);
                    $this->em->remove($construction);
                    $this->em->flush();
                }
            } else {
                $construction->setTicksLeft($construction->getTicksLeft() - 1);
                $this->em->persist($construction);
            }
        }

        $this->em->flush();
        $output->writeln("Constructions End");
    }

    /**
     * @param OutputInterface $output
     */
    protected function doResources(OutputInterface $output)
    {
        $output->writeln("Resources Begin");
        $villageHasBuildings = $this->em->getRepository("DataBundle:VillageHasBuilding")->findAll();
        $resources = $this->em->getRepository('DataBundle:Resource')->findAll();

        $model = new ConstructibleModel();

        /** @var VillageHasBuilding $villageHasBuilding */
        foreach ($villageHasBuildings as $villageHasBuilding) {

            $villageProduction = array();

            /** @var Resource $resource */
            foreach ($resources as $resource) {
                $villageProduction[$resource->getName()] = 0;
            }

            $production = $model->getConstructibleProductionForLevelResourceArray($villageHasBuilding->getConstructible(), $villageHasBuilding->getLevel());
            foreach ($production as $resourceName => $amount) {
                $villageProduction[$resourceName] = $amount;
            }

            /** @var VillageHasResource $villageHasResource */
            foreach ($villageHasBuilding->getVillage()->getVillageHasResources() as $villageHasResource) {
                if (isset($villageProduction[$villageHasResource->getResource()->getName()])) {
                    $villageHasResource->setAmount($villageHasResource->getAmount() + $villageProduction[$villageHasResource->getResource()->getName()]);
                    $this->em->persist($villageHasResource);
                }
            }
        }

        $this->em->flush();
        $output->writeln("Resources Done");
    }

} 