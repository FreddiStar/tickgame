<?php
/**
 * Created by PhpStorm.
 * User: frederiktrojahn
 * Date: 30.08.15
 * Time: 15:00
 */

namespace AppBundle\Controller;

use AppBundle\Model\ConstructibleModel;
use DataBundle\Entity\ConstructibleCostsResource;
use Doctrine\ORM\EntityManager;
use Proxies\__CG__\DataBundle\Entity\Constructible;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/dev")
 */
class DevController extends BaseController
{

    /**
     * @Route("/points/{id}", name="dev_points")
     */
    public function pointsAction($id = -1)
    {
        $data = array();

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $constRepo = $em->getRepository("DataBundle:Constructible");

        $all = $constRepo->findBy(array(), array('name' => 'ASC'));

        $model = new ConstructibleModel();

        $data['constructibles'] = $all;

        if ($id !== -1) {
            /** @var Constructible $focused */
            $focused = $constRepo->find($id);

            for ($i = 1; $i < 30; ++$i) {
                $data["costs"][] = $model->getConstructibleCostForLevelResourceArray($focused, $i);
            }

            $data['focused'] = $focused;

        }

        return $this->render("dev/points.html.twig", $data);
    }

    /**
     * @Route("/formation", name="dev_formations")
     */
    public function formationAction()
    {
        $data = array();
        return $this->render("dev/formation.html.twig", $data);
    }

} 