<?php
/**
 * Created by PhpStorm.
 * User: frederiktrojahn
 * Date: 12.09.15
 * Time: 11:16
 */

namespace AppBundle\Controller;


use AppBundle\Model\ConstructibleModel;
use AppBundle\Model\ResourceModel;
use Doctrine\ORM\EntityManager;

abstract class ConstructibleController extends BaseController
{
    /**
     * @return int
     */
    protected function getConstructionCountForVillageAndPlayer($village, $player)
    {
        return sizeof($this->getRepo('DataBundle:Construction')->findBy(array('village' => $village, 'player' => $player)));;
    }

    abstract protected function getConstructionCount();

    /**
     * @param $id
     * @param $redirectTo
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function doConstruction($id, $redirectTo)
    {
        $constructibleModel = new ConstructibleModel();
        $resourceModel = new ResourceModel();

        $constructibleRepo = $this->getRepo("DataBundle:Constructible");
        $constructibleHasDependencyRepo = $this->getRepo("DataBundle:ConstructibleHasDependency");

        /** @var Constructible $constructible */
        $constructible = $constructibleRepo->find($id);

        if (!$constructible) {
            return $this->redirectToRoute($redirectTo);
        }

        /** @var Village $village */
        $village = $this->focusedVillage();
        $techtree = $constructibleModel->getVillageTechTree($this->player(), $village);

        list($buildable, $missingDependencies) = $constructibleModel->canStartConstruction($constructible,
            $constructibleModel->getDependencyTree($constructibleHasDependencyRepo->findAll()),
            $techtree,
            $constructibleModel->getConstructibleNames($constructibleRepo->findAll()));

        $level = $constructibleModel->getNextConstructibleLevel($id, $techtree);

        if ($buildable) {
            $buildable = $constructibleModel->villageHasResourcesForConstructibleLevel($constructible, $level, $village);
        }

        if ($buildable) {
            $constructionCount = $this->getConstructionCount();
            $buildable &= $constructionCount === 0;
        }

        if ($buildable) {
            $em = $this->em();

            $costs = $constructibleModel->getConstructibleCostForLevelResourceArray($constructible, $level);
            $resourceModel->removeResourcesFromVillage($costs, $this->focusedVillage(), $em);

            $this->startConstruction($constructibleModel, $constructible, $em);

            $this->addFlash('notice', 'construction_started');
        } else {
            $this->addFlash('error', 'construction_failed');
        }

        return $this->redirectToRoute($redirectTo);
    }

    /**
     * @param $constructibleModel
     * @param $constructible
     * @param $em
     */
    abstract protected function startConstruction(ConstructibleModel $constructibleModel, $constructible, $em);

}