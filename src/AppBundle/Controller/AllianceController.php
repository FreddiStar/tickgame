<?php
namespace AppBundle\Controller;

use DataBundle\Entity\Village;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/alliance")
 */
class AllianceController extends BaseController
{

    /**
     * @Route("", name="alliance_index")
     */
    public function indexAction()
    {
        $data = array();
        return $this->render('game/alliance/no_alliance.html.twig', $data);
    }

} 