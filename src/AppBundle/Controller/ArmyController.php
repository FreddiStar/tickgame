<?php
namespace AppBundle\Controller;

use AppBundle\Model\ArmyModel;
use DataBundle\Entity\Army;
use DataBundle\Entity\PlayerHasUnit;
use DataBundle\Entity\Village;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/army")
 */
class ArmyController extends BaseController
{
    /**
     * @Route("", name="army_index")
     */
    public function indexAction()
    {
        $data = array();
        $armyRepo = $this->getRepo('DataBundle:Army');

        $data['armies'] = $armyRepo->findBy(array('player' => $this->player()));

        return $this->render('game/army/army.html.twig', $data);
    }

    /**
     * @Route("/create", name="army_create")
     */
    public function createAction(Request $request)
    {
        $data = array();

        $army = new Army();
        $army->setPlayer($this->player());

        $form = $this->createFormBuilder($army)
            ->add('name')
            ->add('save', 'submit', array('label' => 'Erstellen'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid()) {

            $army->setX($this->focusedVillage()->getX());
            $army->setY($this->focusedVillage()->getY());

            /** @var EntityManager $em */
            $em = $this->em();
            $em->persist($army);
            $em->flush();

            return $this->redirectToRoute('army_index');

        } else {
            foreach ($form->getErrors() as $error) {
                echo $error->getMessage();
            }
        }

        $data['form'] = $form->createView();

        return $this->render('game/army/create.html.twig', $data);
    }

    /**
     * @Route("/manage/{id}", name="army_manage_units")
     */
    public function manageUnitsAction($id)
    {
        $data = array();

        $armyRepo = $this->getRepo('DataBundle:Army');
        /** @var Army $army */
        $army = $armyRepo->findOneBy(array('player' => $this->player(), 'id' => $id));

        if ($army === null
            || $army->getX() != $this->focusedVillage()->getX()
            || $army->getY() != $this->focusedVillage()->getY()
        ) {
            $this->addFlash('error', 'army_not_in_focused_village');
            return $this->redirectToRoute('army_index');
        }

        $data['army'] = $army;

        return $this->render('game/army/manage_units.html.twig', $data);
    }

    /**
     * @Route("/move_unit_to_army/{playerHasUnitId}/{armyId}", name="army_move_unit_to_army")
     */
    public function moveUnitToArmyAction($playerHasUnitId, $armyId)
    {
        $playerHasUnitRepo = $this->getRepo('DataBundle:PlayerHasUnit');
        $armyRepo = $this->getRepo('DataBundle:Army');

        $playerUnit = $playerHasUnitRepo->findOneBy(array('id' => $playerHasUnitId, 'player' => $this->player()));
        $army = $armyRepo->findOneBy(array('id' => $armyId, 'player' => $this->player()));

        if ($playerUnit === null || $army === null) {
            $this->addFlash('error', 'army_or_unit_dont_belong_to_you');
            return $this->redirectToRoute('army_index');
        }

        if ($this->focusedVillage()->getX() != $army->getX() || $this->focusedVillage()->getY() != $army->getY()) {
            $this->addFlash('error', 'army_not_on_current_village');
            return $this->redirectToRoute('army_index');
        }

        ArmyModel::moveUnitToArmy($playerUnit, $army, $this->em());
        return $this->redirectToRoute('army_manage_units', array('id' => $armyId));
    }

    /**
     * @Route("/move_unit_to_village/{playerHasUnitId}/{armyId}", name="army_move_unit_to_village")
     */
    public function moveUnitToVillageAction($playerHasUnitId, $armyId)
    {
        $playerHasUnitRepo = $this->getRepo('DataBundle:PlayerHasUnit');

        /** @var PlayerHasUnit $playerUnit */
        $playerUnit = $playerHasUnitRepo->findOneBy(array('id' => $playerHasUnitId, 'player' => $this->player()));

        if ($playerUnit === null) {
            $this->addFlash('error', 'unit_dont_belong_to_you');
            return $this->redirectToRoute('army_index');
        }

        $army = $playerUnit->getArmyHasUnit()->getArmy();

        if ($this->focusedVillage()->getX() != $army->getX() || $this->focusedVillage()->getY() != $army->getY()) {
            $this->addFlash('error', 'army_not_on_current_village');
            return $this->redirectToRoute('army_index');
        }

        ArmyModel::moveUnitToVillage($playerUnit, $this->focusedVillage(), $this->em());
        return $this->redirectToRoute('army_manage_units', array('id' => $armyId));
    }
} 