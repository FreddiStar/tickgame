<?php

namespace AppBundle\Controller;


use DataBundle\Entity\Config;
use DataBundle\Entity\Player;
use DataBundle\Entity\Village;
use DataBundle\Entity\VillageHasResource;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class BaseController extends Controller
{
    private $config;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->initConfig();
    }


    /**
     * @param $name
     * @return mixed
     */
    public function config($name)
    {
        if (isset($this->config[$name])) {
            return $this->config[$name];
        }

        return null;
    }

    /**
     * @param $name
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepo($name)
    {
        return $this->getDoctrine()->getRepository($name);
    }


    /**
     * @return Player
     */
    public function player()
    {
        /** @var Player $player */
        $player = $this->get('security.context')->getToken()->getUser();
        if (!$player instanceof Player) {
            return null;
        }
        return $player;
    }

    /**
     * @return EntityManager
     */
    protected function em()
    {
        $em = $this->get('doctrine')->getManager();
        return $em;
    }

    /**
     * @return Village
     */
    public function focusedVillage()
    {
        /** @var Player $player */
        $player = $this->player();

        /** @var Village $focusedVillage */
        $focusedVillage = null;
        if ($player !== null) {
            /** @var Village $village */
            foreach ($player->getVillages() as $village) {
                if ($village->getFocused()) {
                    $focusedVillage = $village;
                }
            }
        }

        return $focusedVillage;
    }

    public function render($view, array $parameters = array(), Response $response = null)
    {
        $player = $this->player();

        if ($player !== NULL) {

            $parameters['_player'] = $player;
            $village = $this->focusedVillage();
            $parameters['_focused_village']['village'] = $village;

            /** @var VillageHasResource $villageHasResource */
            foreach ($village->getVillageHasResources() as $villageHasResource) {
                $parameters['_focused_village'][$villageHasResource->getResource()->getName()] = $villageHasResource->getAmount();
            }
        }

        return parent::render($view, $parameters, $response);
    }

    protected function initConfig()
    {
        $this->config = array();

        /** @var Config $c */
        foreach ($this->getRepo('DataBundle:Config')->findAll() as $c) {
            $this->config[$c->getKey()] = $c->getValue();
        }
    }


}