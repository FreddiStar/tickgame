<?php
/**
 * Created by PhpStorm.
 * User: frederiktrojahn
 * Date: 15.09.15
 * Time: 12:12
 */

namespace AppBundle\Controller;

use DataBundle\Entity\Constructible;
use DataBundle\Entity\PlayerHasUnit;
use DataBundle\Entity\VillageHasUnit;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Model\ConstructibleModel;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/unit")
 */
class UnitController extends BaseController
{

    /**
     * @Route("/", name="unit_index")
     */
    public function indexAction(Request $request)
    {
        $parameters = array();

        $repo = $this->getRepo("DataBundle:Constructible");

        $model = new ConstructibleModel();

        // TODO: For testing only!
        if (in_array($this->container->get( 'kernel' )->getEnvironment(), array('dev', 'test'))) {
            foreach ($request->request as $key => $amount) {
                $keyParts = explode('_', $key);
                if ($keyParts[0] == 'unit') {
                    $amount = (int)$amount;
                    $unitId = $keyParts[1];

                    $unit = $repo->find($unitId);

                    /** @var EntityManager $em */
                    $em = $this->em();

                    for ($i = 0; $i < $amount; ++$i) {
                        $playerHasUnit = new PlayerHasUnit();
                        $playerHasUnit->setPlayer($this->player());
                        $playerHasUnit->setConstructible($unit);

                        $em->persist($playerHasUnit);
                        $em->flush();

                        $villageHasUnit = new VillageHasUnit();
                        $villageHasUnit->setPlayerHasUnit($playerHasUnit);
                        $villageHasUnit->setVillage($this->focusedVillage());
                        $em->persist($villageHasUnit);
                        $em->flush();
                    }
                }
            }
        }

        $dependencies = $model->getDependencyTree($this->getRepo("DataBundle:ConstructibleHasDependency")->findAll());
        $techtree = $model->getVillageTechTree($this->player(), $this->focusedVillage());

        $constructibles = $repo->findBy(array('type' => 'unit'), array('order' => 'ASC'));

        $units = array();

        /** @var Constructible $unit */
        foreach ($constructibles as $unit) {
            $data = array();
            $data['id'] = $unit->getId();
            $data['name'] = $unit->getName();
            $data['cost'] = $model->getConstructibleCostForLevelResourceArray($unit, 1);
            $units[] = $unit;
        }

        $parameters['units'] = $units;

        return $this->render("game/units/units.html.twig", $parameters);
    }

}