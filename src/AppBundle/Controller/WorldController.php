<?php
namespace AppBundle\Controller;

use DataBundle\Entity\Village;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/world")
 */
class WorldController extends BaseController
{
    /**
     * @Route("", name="world_index")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('world_chunk',
            array('x' => $this->focusedVillage()->getX(),
                'y' => $this->focusedVillage()->getY()));
    }

    /**
     * @Route("/chunk/{x}/{y}", name="world_chunk")
     */
    public function chunkAction($x, $y)
    {
        $world = array();
        /** @var EntityRepository $villageRepo */
        $villageRepo = $this->getRepo('DataBundle:Village');

        $size = 5;

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $villageRepo->createQueryBuilder('v');
        $query = $queryBuilder
            ->where('v.x > :x-:size')
            ->andWhere('v.x <= :x+:size')
            ->andwhere('v.y > :y-:size')
            ->andWhere('v.y <= :y+:size')
            ->setParameter('size', $size)
            ->setParameter('x', $x)
            ->setParameter('y', $y)->getQuery();

        for ($xx = $x - $size; $xx <= $x + $size; ++$xx) {
            for ($yy = $y - $size; $yy <= $y + $size; ++$yy) {
                $world[$yy][$xx] = "";
            }
        }

        /** @var Village $village */
        foreach ($query->getResult() as $village) {
            $world[$village->getY()][$village->getX()] = $village->getName();
        }

        $data = array();
        $data['world'] = $world;
        return $this->render('game/world/world.html.twig', $data);
    }

} 