<?php
/**
 * Created by PhpStorm.
 * User: frederiktrojahn
 * Date: 08.08.15
 * Time: 15:20
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends BaseController
{

    /**
     * @Route("/admin")
     */
    public function indexAction(Request $request)
    {
        return new Response("Admin");
    }

} 