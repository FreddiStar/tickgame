<?php
namespace AppBundle\Controller;

use AppBundle\Model\ConstructibleModel;
use AppBundle\Model\ResourceModel;
use DataBundle\Entity\Constructible;
use DataBundle\Entity\Construction;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/research")
 */
class ResearchController extends ConstructibleController
{

    private function buildResearchData()
    {
        $model = new ConstructibleModel();

        $dependencies = $model->getDependencyTree($this->getRepo("DataBundle:ConstructibleHasDependency")->findAll());
        $techtree = $model->getVillageTechTree($this->player(), $this->focusedVillage());

        $repo = $this->getRepo("DataBundle:Constructible");
        $constructibles = $repo->findBy(array('type' => 'research'), array('order' => 'ASC'));

        $constructionCount = $this->getConstructionCount();

        $buildingData = array();
        $constructibleNames = $model->getConstructibleNames($repo->findAll());

        /** @var Constructible $constructible */
        foreach ($constructibles as $constructible) {

            $data = array();
            $data['id'] = $constructible->getId();
            $data['name'] = $constructible->getName();
            $data['description'] = $constructible->getDescription();
            if (isset($techtree[$constructible->getId()])) {
                $data['level'] = $techtree[$constructible->getId()];
            } else {
                $data['level'] = 0;
            }

            $data['cost'] = $model->getConstructibleCostForLevelResourceArray($constructible, $data['level'] + 1);

            list($buildable, $missingDependencies) = $model->canStartConstruction($constructible, $dependencies, $techtree, $constructibleNames);

            $enoughResources = $model->villageHasResourcesForConstructibleLevel($constructible, $data['level'] + 1, $this->focusedVillage());

            $buildable &= $enoughResources;
            $buildable &= ($constructionCount === 0);

            $data['enough_resources'] = $enoughResources;
            $data['construction_count'] = $constructionCount;
            $data['missing_dependencies'] = $missingDependencies;
            $data['buildable'] = $buildable;

            $buildingData[] = $data;
        }

        return $buildingData;
    }

    /**
     * @Route("/", name="research_index")
     */
    public function indexAction(Request $request)
    {
        $parameter = array();

        $parameter['researches'] = $this->buildResearchData();
        $parameter['constructions'] = $this->getRepo("DataBundle:Construction")->findBy(
            array('village' => null, 'player' => $this->player()));

        return $this->render('game/research/research.html.twig', $parameter);
    }

    /**
     * @Route("/research/{id}", name="do_research")
     */
    public function buildAction($id, Request $request)
    {
        return $this->doConstruction($id, 'research_index');
    }

    protected function getConstructionCount()
    {
        return $this->getConstructionCountForVillageAndPlayer(null, $this->player());
    }

    /**
     * @param $constructibleModel
     * @param $constructible
     * @param $em
     */
    protected function startConstruction(ConstructibleModel $constructibleModel, $constructible, $em)
    {
        $constructibleModel->startConstruction($constructible, $this->player(), NULL, $em);
    }
}