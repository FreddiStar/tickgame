<?php
namespace AppBundle\Controller;

use AppBundle\Model\ConstructibleModel;
use AppBundle\Model\ResourceModel;
use DataBundle\Entity\Constructible;
use DataBundle\Entity\ConstructibleCostsResource;
use DataBundle\Entity\Village;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/buildings")
 */
class BuildingController extends ConstructibleController
{

    private function buildBuildingData()
    {
        $model = new ConstructibleModel();

        $dependencies = $model->getDependencyTree($this->getRepo("DataBundle:ConstructibleHasDependency")->findAll());
        $techtree = $model->getVillageTechTree($this->player(), $this->focusedVillage());

        $repo = $this->getRepo("DataBundle:Constructible");
        $constructibles = $repo->findBy(array('type' => 'building'), array('order' => 'ASC'));

        $buildingData = array();
        $all = $repo->findAll();

        $constructionCount = $this->getConstructionCount();

        $constructibleNames = $model->getConstructibleNames($all);

        /** @var Constructible $constructible */
        foreach ($constructibles as $constructible) {

            $data = array();
            $data['id'] = $constructible->getId();
            $data['name'] = $constructible->getName();
            $data['description'] = $constructible->getDescription();
            if (isset($techtree[$constructible->getId()])) {
                $data['level'] = $techtree[$constructible->getId()];
            } else {
                $data['level'] = 0;
            }

            $data['cost'] = $model->getConstructibleCostForLevelResourceArray($constructible, $data['level'] + 1);

            list($buildable, $missingDependencies) = $model->canStartConstruction($constructible, $dependencies, $techtree, $constructibleNames);

            $enoughResources = $model->villageHasResourcesForConstructibleLevel($constructible, $data['level'] + 1, $this->focusedVillage());

            $buildable &= $enoughResources;
            $buildable &= ($constructionCount === 0);

            $data['enough_resources'] = $enoughResources;
            $data['construction_count'] = $constructionCount;
            $data['missing_dependencies'] = $missingDependencies;
            $data['buildable'] = $buildable;

            $buildingData[] = $data;
        }

        return $buildingData;
    }

    /**
     * @Route("/", name="buildings_index")
     */
    public function indexAction(Request $request)
    {
        $parameter = array();

        $parameter['buildings'] = $this->buildBuildingData();
        $parameter['constructions'] = $this->getRepo("DataBundle:Construction")->findBy(
            array('village' => $this->focusedVillage(), 'player' => $this->player()));

        return $this->render('game/buildings/buildings.html.twig', $parameter);
    }

    /**
     * @Route("/build/{id}", name="build_building")
     */
    public function buildAction($id, Request $request)
    {
        return $this->doConstruction($id, 'buildings_index');
    }


    protected function getConstructionCount()
    {
        return $this->getConstructionCountForVillageAndPlayer($this->focusedVillage(), $this->player());
    }


    /**
     * @param $constructibleModel
     * @param $constructible
     * @param $em
     */
    protected function startConstruction(ConstructibleModel $constructibleModel, $constructible, $em)
    {
        $constructibleModel->startConstruction($constructible, $this->player(), $this->focusedVillage(), $em);
    }
}