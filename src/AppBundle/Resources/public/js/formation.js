var Formation = {
    _units: [],

    addUnit: function (unit) {
        this._units.push(unit);
    }
};

Formation.Unit = function (x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
};

Formation.Renderer = {

    width: 500,
    height: 300,

    init: function (width, height, canvas) {
        this.width = width;
        this.height = height;
        this.canvas = canvas;
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.ctx = canvas.getContext('2d');
    },

    clear: function () {
        this.ctx.fillStyle = "#ddd";
        this.ctx.fillRect(0, 0, this.width, this.height);
    },

    draw: function () {
        this.clear();

        Formation._units.forEach(function (unit, x, y) {
            Formation.Renderer.drawUnit(unit);
        });
    },

    drawUnit: function (unit) {
        this.ctx.fillStyle = "#f00";
        this.ctx.fillRect(unit.x, unit.y, unit.width, unit.height);
    }
};

function randomInRange(min, max) {
    return Math.random() * (max - min) + min;
}

$(document).ready(function () {
    Formation.Renderer.init(600, 300, document.getElementById('formation'));

    for (x = 0; x < 10; ++x) {
        Formation.addUnit(new Formation.Unit(
            randomInRange(0, Formation.Renderer.width),
            randomInRange(0, Formation.Renderer.height),
            randomInRange(10, 30), randomInRange(10, 20)));
    }

    Formation.Renderer.draw();
});
