<?php
namespace AppBundle\Model;


use DataBundle\Entity\Army;
use DataBundle\Entity\ArmyHasUnit;
use DataBundle\Entity\PlayerHasUnit;
use DataBundle\Entity\Village;
use DataBundle\Entity\VillageHasUnit;
use Doctrine\ORM\EntityManager;

class ArmyModel
{

    public static function moveUnitToArmy(PlayerHasUnit $playerHasUnit, Army $army, EntityManager $em)
    {
        $armyHasUnit = new ArmyHasUnit();
        $armyHasUnit->setX(0);
        $armyHasUnit->setY(0);
        $armyHasUnit->setArmy($army);
        $armyHasUnit->setPlayerHasUnit($playerHasUnit);
        $playerHasUnit->setArmyHasUnit($armyHasUnit);

        $em->persist($armyHasUnit);
        $em->flush();

        $em->remove($playerHasUnit->getVillageHasUnit());

        $em->flush();
    }

    public static function moveUnitToVillage(PlayerHasUnit $playerHasUnit, Village $village, EntityManager $em)
    {
        $villageHasUnit = new VillageHasUnit();
        $villageHasUnit->setPlayerHasUnit($playerHasUnit);
        $villageHasUnit->setVillage($village);
        $em->persist($villageHasUnit);
        $em->flush();

        $em->remove($playerHasUnit->getArmyHasUnit());
        $em->flush();
    }

} 