<?php

namespace AppBundle\Model;

use DataBundle\Entity\Constructible;
use DataBundle\Entity\ConstructibleCostsResource;
use DataBundle\Entity\ConstructibleHasDependency;
use DataBundle\Entity\ConstructibleProduction;
use DataBundle\Entity\Construction;
use DataBundle\Entity\Player;
use DataBundle\Entity\PlayerHasResearch;
use DataBundle\Entity\Village;
use DataBundle\Entity\VillageHasBuilding;
use Doctrine\ORM\EntityManager;

class ConstructibleModel
{
    /**
     * @param Player $player
     * @param Village $village
     * @return array
     */
    public function getVillageTechTree(Player $player, Village $village)
    {
        $techtree = array();

        $techtree = $this->getPlayerResearch($player, $techtree);
        $techtree = $this->getVillageBuildings($village, $techtree);

        return $techtree;
    }

    /**
     * @param $all
     * @return array
     */
    public function getConstructibleNames($all)
    {
        $constructibleNames = array();
        /** @var Constructible $c */
        foreach ($all as $c) {
            $constructibleNames[$c->getId()] = $c->getName();
        }
        return $constructibleNames;
    }

    /**
     * @param Player $player
     * @return array
     */
    public function getPlayerTechTree(Player $player)
    {
        $techtree = array();

        $techtree = $this->getPlayerResearch($player, $techtree);

        /** @var Village $village */
        foreach ($player->getVillages() as $village) {
            $buildings = $village->getVillageHasBuildings();
            /** @var VillageHasBuilding $villageHasBuilding */
            foreach ($buildings as $villageHasBuilding) {
                $id = $villageHasBuilding->getConstructible()->getId();
                if (!isset($techtree[$id])) {
                    $techtree[$id] = $villageHasBuilding->getLevel();
                } else {
                    if ($techtree[$id] < $villageHasBuilding->getLevel()) {
                        $techtree[$id] = $villageHasBuilding->getLevel();
                    }
                }
            }
        }

        return $techtree;

    }

    /**
     * @param Player $player
     * @param array $researches
     * @return array
     */
    public function getPlayerResearch(Player $player, $researches = array())
    {
        /** @var PlayerHasResearch $playerResearch */
        foreach ($player->getPlayerHasResearches() as $playerResearch) {
            $researches[$playerResearch->getConstructibleId()] = $playerResearch->getLevel();
        }

        return $researches;
    }

    /**
     * @param Village $village
     * @param array $buildings
     * @return array
     */
    public function getVillageBuildings(Village $village, $buildings = array())
    {
        /** @var VillageHasBuilding $villageBuilding */
        foreach ($village->getVillageHasBuildings() as $villageBuilding) {
            $buildings[$villageBuilding->getConstructibleId()] = $villageBuilding->getLevel();
        }

        return $buildings;
    }

    /**
     * @param $dependencies
     * @return array
     */
    public function getDependencyTree($dependencies)
    {
        $depTree = array();

        /** @var ConstructibleHasDependency $dep */
        foreach ($dependencies as $dep) {
            $depTree[$dep->getConstructibleId()][$dep->getDependencyId()] = $dep->getLevel();
        }

        return $depTree;
    }

    /**
     * @param $id
     * @param $techtree
     * @return int
     */
    public function getNextConstructibleLevel($id, $techtree)
    {
        $level = 1;
        if (isset($techtree[$id])) {
            $level = $techtree[$id] + 1;
            return $level;
        }
        return $level;
    }

    /**
     * @param Constructible $constructible
     * @param Player $player
     * @param Village $village
     * @param EntityManager $em
     */
    public function startConstruction(
        Constructible $constructible,
        Player $player,
        $village,
        EntityManager $em)
    {
        $construction = new Construction();
        $construction->setAmount(1);
        $construction->setConstructible($constructible);

        // TODO: Calculate Ticks Left
        $construction->setTicksLeft(2);
        if ($village !== null) {
            $construction->setVillage($village);
        }
        $construction->setPlayer($player);

        $em->persist($construction);
        $em->flush();
    }

    /**
     * @param Constructible $constructible
     * @param $dependencies
     * @param $techtree
     * @param $constructibleNames
     * @return array
     */
    public function canStartConstruction(Constructible $constructible, $dependencies, $techtree, $constructibleNames)
    {
        $missingDependencies = array();
        $buildable = true;
        if (isset($dependencies[$constructible->getId()])) {
            foreach ($dependencies[$constructible->getId()] as $buildingDependency => $level) {
                if (!isset($techtree[$buildingDependency])) {
                    $buildable = false;
                    $missingDependencies[] = array('name' => $constructibleNames[$buildingDependency], 'level' => $level);
                } else {
                    if ($techtree[$buildingDependency] < $level) {
                        $buildable = false;
                        $missingDependencies[] = array('name' => $constructibleNames[$buildingDependency], 'level' => $level);
                    }
                }
            }
            return array($buildable, $missingDependencies);
        }
        return array($buildable, $missingDependencies);
    }

    public function villageHasResourcesForConstructibleLevel(Constructible $constructible, $level, Village $village)
    {
        $villageModel = new VillageModel();

        $villageResources = $villageModel->getVillageResourceArray($village);
        $constructibleCost = $this->getConstructibleCostForLevelResourceArray($constructible, $level);

        foreach ($constructibleCost as $resource => $amount) {
            if ($villageResources[$resource] < $amount) {
                return false;
            }
        }

        return true;
    }

    public function getConstructibleCostForLevelResourceArray(Constructible $constructible, $level)
    {
        $costs = array();
        $growth = array();

        /** @var ConstructibleCostsResource $constructibleCostResource */
        foreach ($constructible->getConstructibleCostsResources() as $constructibleCostResource) {
            $costs[$constructibleCostResource->getResource()->getName()] = $constructibleCostResource->getAmount();
            $growth[$constructibleCostResource->getResource()->getName()] = $constructibleCostResource->getGrowth();
        }


        return $this->adjustResourcesWithGrowth($costs, $level, $growth);
    }

    public function getConstructibleProductionForLevelResourceArray(Constructible $constructible, $level)
    {
        $production = array();
        $growth = array();

        /** @var ConstructibleProduction $constructibleProduction */
        foreach ($constructible->getConstructibleProductions() as $constructibleProduction) {
            $production[$constructibleProduction->getResource()->getName()] = $constructibleProduction->getAmount();
            $growth[$constructibleProduction->getResource()->getName()] = $constructibleProduction->getGrowth();
        }

        return $this->adjustResourcesWithGrowth($production, $level, $growth);
    }

    /**
     * @param $input
     * @param $level
     * @param $growth
     * @return array
     */
    private function adjustResourcesWithGrowth($input, $level, $growth)
    {
        $result = array();

        foreach ($input as $res => $amount) {
            $result[$res] = floor($amount * pow($growth[$res], $level));
        }

        return $result;
    }


}