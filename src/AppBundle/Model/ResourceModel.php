<?php
/**
 * Created by PhpStorm.
 * User: frederiktrojahn
 * Date: 05.09.15
 * Time: 18:11
 */

namespace AppBundle\Model;


use DataBundle\Entity\Village;
use DataBundle\Entity\VillageHasResource;
use Doctrine\ORM\EntityManager;

class ResourceModel
{
    /**
     * @param $resources array Array[Resource-ID] => Amount
     * @param Village $village
     * @param EntityManager $em
     */
    public function removeResourcesFromVillage($resources, Village $village, EntityManager $em)
    {
        /** @var VillageHasResource $villageHasResource */
        foreach ($village->getVillageHasResources() as $villageHasResource) {
            if (isset($resources[$villageHasResource->getResource()->getName()])) {
                $villageHasResource->setAmount($villageHasResource->getAmount() - $resources[$villageHasResource->getResource()->getName()]);
            }
        }

        $em->persist($village);
        $em->flush();
    }

} 