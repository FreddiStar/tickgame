<?php
/**
 * Created by PhpStorm.
 * User: frederiktrojahn
 * Date: 05.09.15
 * Time: 15:28
 */

namespace AppBundle\Model;


use DataBundle\Entity\Village;
use DataBundle\Entity\VillageHasResource;

class VillageModel
{

    public function getVillageResourceArray(Village $village)
    {
        $result = array();

        /** @var VillageHasResource $villageHasResource */
        foreach ($village->getVillageHasResources() as $villageHasResource) {
            $result[$villageHasResource->getResource()->getName()] = $villageHasResource->getAmount();
        }

        return $result;

    }

} 