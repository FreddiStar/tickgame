<?php

use DataBundle\Entity\ArmorType;
use DataBundle\Entity\Constructible;
use DataBundle\Entity\ConstructibleCostsResource;
use DataBundle\Entity\ConstructibleHasDependency;
use DataBundle\Entity\ConstructibleProduction;
use DataBundle\Entity\DamageType;
use DataBundle\Entity\Player;
use DataBundle\Entity\Resource;
use DataBundle\Entity\UnitData;
use DataBundle\Entity\Village;
use DataBundle\Entity\VillageHasResource;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class GameFixtures implements FixtureInterface
{
    // User
    private $admin;
    private $player;

    // Resources
    private $resource_stone;
    private $resource_wood;
    private $resource_coal;
    private $resource_iron;

    // Armor and Damage types
    private $armor_leather;
    private $armor_plate;
    private $armor_chain;
    private $armor_fabric;
    private $armor_siege;

    private $damage_blunt;
    private $damage_arrow;
    private $damage_bolt;
    private $damage_sword;
    private $damage_spear;
    private $damage_axe;
    private $damage_siege;

    // Buildings
    private $building_lumberjack;
    private $building_storage;
    private $building_defenses;
    private $building_barracks;
    private $building_blacksmith;
    private $building_stable;
    private $building_market;
    private $building_housing;
    private $building_farm;
    private $building_mine;
    private $building_charcoalburner;
    private $building_quarry;

    // Forschungen
    private $research_crop_rotation;
    private $research_siege_weapons;
    private $research_chain_armor;
    private $research_plate_armor;
    private $research_leather_armor;
    private $research_spears;
    private $research_axes;
    private $research_swords;
    private $research_crossbows;
    private $research_bow_and_arrow;


    // Einheiten
    private $unit_catapult;
    private $unit_ram;
    private $unit_heave_cavalry;
    private $unit_light_cavalry;
    private $unit_scout;
    private $unit_knight;
    private $unit_spearman;
    private $unit_axeman;
    private $unit_swordsman;
    private $unit_crossbowman;
    private $unit_archer;


    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->createResources($manager);

        $this->createPlayers($manager);

        $this->createDamageAndArmor($manager);

        $this->createBuildings($manager);
        $this->createResearches($manager);
        $this->createUnits($manager);

        $this->createBuildingDependencies($manager);
        $this->createBuildingProductions($manager);

        $manager->flush();
    }

    private function createBuildingProductions(ObjectManager $manager)
    {
        $this->createBuildingProduction($this->building_mine, $this->resource_iron, 1, $manager);
        $this->createBuildingProduction($this->building_quarry, $this->resource_stone, 1, $manager);
        $this->createBuildingProduction($this->building_charcoalburner, $this->resource_coal, 1, $manager);
        $this->createBuildingProduction($this->building_lumberjack, $this->resource_wood, 1, $manager);
    }

    private function createBuildingProduction(Constructible $building, Resource $resource, $amount, ObjectManager $manager)
    {
        echo $building->getName() . " produces " . $resource->getName() . " " . $amount . "\n";

        $production = new ConstructibleProduction();
        $production->setConstructible($building);
        $production->setResource($resource);
        $production->setAmount($amount);
        $production->setGrowth(1.05);

        $manager->persist($production);
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @return \DataBundle\Entity\Player
     */
    private function createAdminUser(ObjectManager $manager)
    {
        $this->admin = $this->createPlayer('admin', '$2a$04$V4b4InBxuz8oTh78vIofxOSGEncP5A4BXVXK7LBVlckddHrHNBKfO', 'admin@server.de', $manager);
        $this->createVillage('admin village', 0, 0, $this->admin, $manager);
    }

    /**
     * @param ObjectManager $manager
     * @return Player
     */
    private function createPlayer($name, $password, $email, ObjectManager $manager)
    {
        $player = new Player();
        $player->setName($name);
        $player->setPassword($password);
        $player->setEmail($email);
        $player->setSalt("salt");
        $player->setLastLogin(0);

        $manager->persist($player);
        $manager->flush();
        return $player;
    }

    /**
     * @param ObjectManager $manager
     */
    private function createBuildingDependencies(ObjectManager $manager)
    {
        $this->createConstructibleDependency($this->building_blacksmith, $this->building_charcoalburner, 4, $manager);
    }

    /**
     * @param Constructible $building
     * @param $level
     * @param Constructible $dependsOn
     * @param $dep_level
     * @param ObjectManager $manager
     * @return ConstructibleHasDependency
     */
    private function createConstructibleDependency(
        Constructible $building,
        Constructible $dependsOn,
        $level, ObjectManager $manager)
    {
        echo $building->getName() . " depends on " . $dependsOn->getName() . " (" . $level . ")\n";

        /** @var ConstructibleHasDependency $dep */
        $dep = new ConstructibleHasDependency();
        $dep->setLevel($level);
        $dep->setConstructibleRelatedByDependencyId($dependsOn);
        $dep->setConstructibleRelatedByConstructibleId($building);
        $dep->setConstructibleId($building->getId());
        $dep->setDependencyId($dependsOn->getId());

        $manager->persist($dep);
        $manager->persist($building);
        $manager->persist($dependsOn);
        $manager->flush();

        return $dep;
    }

    /**
     * @param $village
     * @param $resource
     * @param $amount
     * @param ObjectManager $manager
     */
    private function addResourceToVillage($village, $resource, $amount, ObjectManager $manager)
    {

        $villageHasResource = new VillageHasResource();
        $villageHasResource->setVillage($village);
        $villageHasResource->setResource($resource);
        $villageHasResource->setAmount($amount);

        $manager->persist($villageHasResource);
        $manager->flush();
    }

    /**
     * @param $name
     * @param ObjectManager $manager
     * @return Resource
     */
    private function createResource($name, ObjectManager $manager)
    {
        $res = new Resource();
        $res->setName($name);

        $manager->persist($res);
        $manager->flush();

        return $res;
    }

    /**
     * @param $name
     * @param ObjectManager $manager
     * @return ArmorType
     */
    private function createArmorType($name, ObjectManager $manager)
    {
        $armor = new ArmorType();
        $armor->setName($name);

        $manager->persist($armor);
        $manager->flush();

        return $armor;
    }

    /**
     * @param $name
     * @param ObjectManager $manager
     * @return DamageType
     */
    private function createDamageType($name, ObjectManager $manager)
    {
        $type = new DamageType();
        $type->setName($name);
        $manager->persist($type);

        return $type;
    }

    /**
     * @param DamageType $damage
     * @param ArmorType $armor
     * @param float $factor
     * @param ObjectManager $manager
     */
    private function setDamageFactorVsArmor(DamageType $damage, ArmorType $armor, $factor, ObjectManager $manager)
    {
        $vs = new \DataBundle\Entity\DamageVsArmor();
        $vs->setArmorType($armor);
        $vs->setDamageType($damage);
        $vs->setFactor($factor);

        $manager->persist($vs);
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    private function createResources(ObjectManager $manager)
    {
        $this->resource_wood = $this->createResource("wood", $manager);
        $this->resource_stone = $this->createResource("stone", $manager);
        $this->resource_coal = $this->createResource("coal", $manager);
        $this->resource_iron = $this->createResource("iron", $manager);
    }

    /**
     * @param ObjectManager $manager
     */
    private function createDamageAndArmor(ObjectManager $manager)
    {
        $this->armor_leather = $this->createArmorType("leather", $manager);
        $this->armor_plate = $this->createArmorType("plate", $manager);
        $this->armor_chain = $this->createArmorType("chain", $manager);
        $this->armor_fabric = $this->createArmorType("fabric", $manager);
        $this->armor_siege = $this->createArmorType("siege", $manager);

        $this->damage_blunt = $this->createDamageType("blunt", $manager);
        $this->damage_arrow = $this->createDamageType("arrow", $manager);
        $this->damage_bolt = $this->createDamageType("bolt", $manager);
        $this->damage_sword = $this->createDamageType("sword", $manager);
        $this->damage_spear = $this->createDamageType("spear", $manager);
        $this->damage_axe = $this->createDamageType("axe", $manager);
        $this->damage_siege = $this->createDamageType("siege", $manager);
    }

    private function createConstructible($name, $type, $unitData, $order, ObjectManager $manager)
    {
        $c = new Constructible();
        $c->setName($name);
        $c->setDescription("Empty for now");
        $c->setType($type);
        $c->setOrder($order);
        if ($unitData !== null) {
            $c->setUnitData($unitData);
        }

        $manager->persist($c);
        $manager->flush();

        echo "Created " . $type . ": " . $c->getName() . "\n";

        return $c;
    }

    private function createBuildings(ObjectManager $manager)
    {
        $this->building_lumberjack = $this->createConstructible('lumberjack', 'building', null, 0, $manager);
        $this->createConstructibleCostWithData($this->building_lumberjack, 5, 5, 5, 5, $manager);

        $this->building_quarry = $this->createConstructible('quarry', 'building', null, 2, $manager);
        $this->createConstructibleCostWithData($this->building_quarry, 5, 5, 5, 5, $manager);

        $this->building_charcoalburner = $this->createConstructible('charcoalburner', 'building', null, 3, $manager);
        $this->createConstructibleCostWithData($this->building_charcoalburner, 5, 5, 5, 5, $manager);

        $this->building_mine = $this->createConstructible('mine', 'building', null, 4, $manager);
        $this->createConstructibleCostWithData($this->building_mine, 5, 5, 5, 5, $manager);

        $this->building_farm = $this->createConstructible('farm', 'building', null, 1, $manager);
        $this->createConstructibleCostWithData($this->building_farm, 5, 5, 5, 5, $manager);

        $this->building_housing = $this->createConstructible('housing', 'building', null, 5, $manager);
        $this->createConstructibleCostWithData($this->building_housing, 5, 5, 5, 5, $manager);

        $this->building_market = $this->createConstructible('market', 'building', null, 6, $manager);
        $this->createConstructibleCostWithData($this->building_market, 5, 5, 5, 5, $manager);

        $this->building_stable = $this->createConstructible('stable', 'building', null, 7, $manager);
        $this->createConstructibleCostWithData($this->building_stable, 5, 5, 5, 5, $manager);

        $this->building_blacksmith = $this->createConstructible('blacksmith', 'building', null, 8, $manager);
        $this->createConstructibleCostWithData($this->building_blacksmith, 5, 5, 5, 5, $manager);

        $this->building_barracks = $this->createConstructible('barracks', 'building', null, 9, $manager);
        $this->createConstructibleCostWithData($this->building_barracks, 5, 5, 5, 5, $manager);

        $this->building_defenses = $this->createConstructible('defenses', 'building', null, 10, $manager);
        $this->createConstructibleCostWithData($this->building_defenses, 5, 5, 5, 5, $manager);

        $this->building_storage = $this->createConstructible('storage', 'building', null, 11, $manager);
        $this->createConstructibleCostWithData($this->building_storage, 5, 5, 5, 5, $manager);


    }

    private function createConstructibleCostWithData(Constructible $constructible, $wood, $stone, $coal, $iron, ObjectManager $manager)
    {
        $this->createConstructibleCost($constructible, $this->resource_wood, $wood, $manager);
        $this->createConstructibleCost($constructible, $this->resource_stone, $stone, $manager);
        $this->createConstructibleCost($constructible, $this->resource_coal, $coal, $manager);
        $this->createConstructibleCost($constructible, $this->resource_iron, $iron, $manager);
    }

    private function createConstructibleCost(Constructible $constructible,
                                             Resource $res, $amount, ObjectManager $manager)
    {
        $cost = new ConstructibleCostsResource();
        $cost->setConstructible($constructible);
        $cost->setResource($res);
        $cost->setAmount($amount);
        $cost->setGrowth(1.05);

        $manager->persist($cost);
        $manager->flush();
    }

    private function createResearches(ObjectManager $manager)
    {
        $this->research_bow_and_arrow = $this->createConstructible('bow_and_arrow', 'research', null, 0, $manager);
        $this->research_crossbows = $this->createConstructible('crossbows', 'research', null, 0, $manager);
        $this->research_swords = $this->createConstructible('swords', 'research', null, 0, $manager);
        $this->research_axes = $this->createConstructible('axes', 'research', null, 0, $manager);
        $this->research_spears = $this->createConstructible('spears', 'research', null, 0, $manager);
        $this->research_leather_armor = $this->createConstructible('leather_armor', 'research', null, 0, $manager);
        $this->research_plate_armor = $this->createConstructible('plate_armor', 'research', null, 0, $manager);
        $this->research_chain_armor = $this->createConstructible('chain_armor ', 'research', null, 0, $manager);
        $this->research_siege_weapons = $this->createConstructible('siege_weapons', 'research', null, 0, $manager);
        $this->research_crop_rotation = $this->createConstructible('crop_rotation', 'research', null, 0, $manager);
    }

    /**
     * @param $armor
     * @param ArmorType $armorType
     * @param $damage
     * @param DamageType $damageType
     * @param $health
     * @param $speed
     * @param $cargo
     * @param ObjectManager $manager
     * @return UnitData
     */
    private function createUnitData($armor,
                                    ArmorType $armorType,
                                    $damage,
                                    DamageType $damageType,
                                    $health,
                                    $speed,
                                    $cargo,
                                    ObjectManager $manager)
    {
        $data = new UnitData();
        $data->setArmorType($armorType);
        $data->setDamageType($damageType);
        $data->setArmor($armor);
        $data->setDamage($damage);
        $data->setCargo($cargo);
        $data->setHealth($health);
        $data->setSpeed($speed);

        $manager->persist($data);
        $manager->flush();

        return $data;
    }

    /**
     * @param ObjectManager $manager
     */
    private function createUnits(ObjectManager $manager)
    {
        $this->unit_archer = $this->createConstructible('archer', 'unit',
            $this->createUnitData(1, $this->armor_fabric, 1, $this->damage_arrow, 1, 1, 1, $manager), 0, $manager);

        $this->unit_crossbowman = $this->createConstructible('crossbowman', 'unit',
            $this->createUnitData(1, $this->armor_leather, 1, $this->damage_bolt, 1, 1, 1, $manager), 0, $manager);

        $this->unit_swordsman = $this->createConstructible('swordsman', 'unit',
            $this->createUnitData(1, $this->armor_leather, 1, $this->damage_sword, 1, 1, 1, $manager), 0, $manager);

        $this->unit_axeman = $this->createConstructible('axeman', 'unit',
            $this->createUnitData(1, $this->armor_chain, 1, $this->damage_axe, 1, 1, 1, $manager), 0, $manager);

        $this->unit_spearman = $this->createConstructible('spearman', 'unit',
            $this->createUnitData(1, $this->armor_leather, 1, $this->damage_spear, 1, 1, 1, $manager), 0, $manager);

        $this->unit_knight = $this->createConstructible('knight', 'unit',
            $this->createUnitData(1, $this->armor_plate, 1, $this->damage_sword, 1, 1, 1, $manager), 0, $manager);

        $this->unit_scout = $this->createConstructible('scout', 'unit',
            $this->createUnitData(1, $this->armor_fabric, 1, $this->damage_sword, 1, 1, 1, $manager), 0, $manager);

        $this->unit_light_cavalry = $this->createConstructible('light_cavalry', 'unit',
            $this->createUnitData(1, $this->armor_leather, 1, $this->damage_sword, 1, 1, 1, $manager), 0, $manager);

        $this->unit_heave_cavalry = $this->createConstructible('heavy_cavalry', 'unit',
            $this->createUnitData(1, $this->armor_plate, 1, $this->damage_spear, 1, 1, 1, $manager), 0, $manager);

        $this->unit_ram = $this->createConstructible('ram', 'unit',
            $this->createUnitData(1, $this->armor_siege, 1, $this->damage_siege, 1, 1, 1, $manager), 0, $manager);

        $this->unit_catapult = $this->createConstructible('catapult', 'unit',
            $this->createUnitData(1, $this->armor_siege, 1, $this->damage_siege, 1, 1, 1, $manager), 0, $manager);
    }

    /**
     * @param ObjectManager $manager
     * @param $player
     */
    private function createVillage($name, $x, $y, $player, ObjectManager $manager)
    {
        $village = new Village();
        $village->setPlayer($player);
        $village->setX($x);
        $village->setY($y);
        $village->setName($name);
        $village->setFocused(true);

        $manager->persist($village);
        $manager->flush();

        $this->addResourceToVillage($village, $this->resource_wood, 100, $manager);
        $this->addResourceToVillage($village, $this->resource_iron, 100, $manager);
        $this->addResourceToVillage($village, $this->resource_coal, 100, $manager);
        $this->addResourceToVillage($village, $this->resource_stone, 100, $manager);
    }

    /**
     * @param ObjectManager $manager
     */
    private function createPlayers(ObjectManager $manager)
    {
        $this->createAdminUser($manager);

        $this->player = $this->createPlayer('player', '$2a$10$Es6GOm3qRoj7rUPaheHe5./lKsDuYubcpoGeZdHChQ88YUzUaWBn6', 'player@server.de', $manager);
        $this->createVillage('player village', 1, 1, $this->player, $manager);
    }


}