<?php

namespace DevBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DevBundle:Default:index.html.twig', array('name' => $name));
    }
}
