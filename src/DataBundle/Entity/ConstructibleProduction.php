<?php

/**
 * Auto generated by MySQL Workbench Schema Exporter.
 * Version 2.1.6-dev (doctrine2-annotation) on 2015-09-27 15:41:22.
 * Goto https://github.com/johmue/mysql-workbench-schema-exporter for more
 * information.
 */

namespace DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DataBundle\Entity\ConstructibleProduction
 *
 * @ORM\Entity()
 * @ORM\Table(name="constructible_production", indexes={@ORM\Index(name="fk_constructible_production_constructible1_idx", columns={"constructible_id"}), @ORM\Index(name="fk_constructible_production_resource1_idx", columns={"resource_id"})})
 */
class ConstructibleProduction
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $amount;

    /**
     * @ORM\Column(type="integer")
     */
    protected $constructible_id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $resource_id;

    /**
     * @ORM\Column(type="float")
     */
    protected $growth;

    /**
     * @ORM\ManyToOne(targetEntity="Constructible", inversedBy="constructibleProductions")
     * @ORM\JoinColumn(name="constructible_id", referencedColumnName="id")
     */
    protected $constructible;

    /**
     * @ORM\ManyToOne(targetEntity="Resource", inversedBy="constructibleProductions")
     * @ORM\JoinColumn(name="resource_id", referencedColumnName="id")
     */
    protected $resource;

    public function __construct()
    {
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \DataBundle\Entity\ConstructibleProduction
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of amount.
     *
     * @param integer $amount
     * @return \DataBundle\Entity\ConstructibleProduction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get the value of amount.
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set the value of constructible_id.
     *
     * @param integer $constructible_id
     * @return \DataBundle\Entity\ConstructibleProduction
     */
    public function setConstructibleId($constructible_id)
    {
        $this->constructible_id = $constructible_id;

        return $this;
    }

    /**
     * Get the value of constructible_id.
     *
     * @return integer
     */
    public function getConstructibleId()
    {
        return $this->constructible_id;
    }

    /**
     * Set the value of resource_id.
     *
     * @param integer $resource_id
     * @return \DataBundle\Entity\ConstructibleProduction
     */
    public function setResourceId($resource_id)
    {
        $this->resource_id = $resource_id;

        return $this;
    }

    /**
     * Get the value of resource_id.
     *
     * @return integer
     */
    public function getResourceId()
    {
        return $this->resource_id;
    }

    /**
     * Set the value of growth.
     *
     * @param float $growth
     * @return \DataBundle\Entity\ConstructibleProduction
     */
    public function setGrowth($growth)
    {
        $this->growth = $growth;

        return $this;
    }

    /**
     * Get the value of growth.
     *
     * @return float
     */
    public function getGrowth()
    {
        return $this->growth;
    }

    /**
     * Set Constructible entity (many to one).
     *
     * @param \DataBundle\Entity\Constructible $constructible
     * @return \DataBundle\Entity\ConstructibleProduction
     */
    public function setConstructible(Constructible $constructible = null)
    {
        $this->constructible = $constructible;

        return $this;
    }

    /**
     * Get Constructible entity (many to one).
     *
     * @return \DataBundle\Entity\Constructible
     */
    public function getConstructible()
    {
        return $this->constructible;
    }

    /**
     * Set Resource entity (many to one).
     *
     * @param \DataBundle\Entity\Resource $resource
     * @return \DataBundle\Entity\ConstructibleProduction
     */
    public function setResource(Resource $resource = null)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Get Resource entity (many to one).
     *
     * @return \DataBundle\Entity\Resource
     */
    public function getResource()
    {
        return $this->resource;
    }

    public function __sleep()
    {
        return array('id', 'amount', 'constructible_id', 'resource_id', 'growth');
    }
}