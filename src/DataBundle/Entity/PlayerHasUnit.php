<?php

/**
 * Auto generated by MySQL Workbench Schema Exporter.
 * Version 2.1.6-dev (doctrine2-annotation) on 2015-09-27 15:41:22.
 * Goto https://github.com/johmue/mysql-workbench-schema-exporter for more
 * information.
 */

namespace DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * DataBundle\Entity\PlayerHasUnit
 *
 * @ORM\Entity()
 * @ORM\Table(name="player_has_unit", indexes={@ORM\Index(name="fk_player_has_unit_player1_idx", columns={"player_id"}), @ORM\Index(name="fk_player_has_unit_constructible1_idx", columns={"constructible_id"})})
 */
class PlayerHasUnit
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $player_id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $constructible_id;

    /**
     * @ORM\OneToOne(targetEntity="ArmyHasUnit", mappedBy="playerHasUnit")
     */
    protected $armyHasUnit;

    /**
     * @ORM\OneToOne(targetEntity="VillageHasUnit", mappedBy="playerHasUnit")
     */
    protected $villageHasUnit;

    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="playerHasUnits")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     */
    protected $player;

    /**
     * @ORM\ManyToOne(targetEntity="Constructible", inversedBy="playerHasUnits")
     * @ORM\JoinColumn(name="constructible_id", referencedColumnName="id")
     */
    protected $constructible;

    public function __construct()
    {
        $this->armyHasUnits = new ArrayCollection();
        $this->villageHasUnits = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \DataBundle\Entity\PlayerHasUnit
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of player_id.
     *
     * @param integer $player_id
     * @return \DataBundle\Entity\PlayerHasUnit
     */
    public function setPlayerId($player_id)
    {
        $this->player_id = $player_id;

        return $this;
    }

    /**
     * Get the value of player_id.
     *
     * @return integer
     */
    public function getPlayerId()
    {
        return $this->player_id;
    }

    /**
     * Set the value of constructible_id.
     *
     * @param integer $constructible_id
     * @return \DataBundle\Entity\PlayerHasUnit
     */
    public function setConstructibleId($constructible_id)
    {
        $this->constructible_id = $constructible_id;

        return $this;
    }

    /**
     * Get the value of constructible_id.
     *
     * @return integer
     */
    public function getConstructibleId()
    {
        return $this->constructible_id;
    }

    /**
     * Set ArmyHasUnit entity (one to one).
     *
     * @param \DataBundle\Entity\ArmyHasUnit $armyHasUnit
     * @return \DataBundle\Entity\PlayerHasUnit
     */
    public function setArmyHasUnit(ArmyHasUnit $armyHasUnit = null)
    {
        $armyHasUnit->setPlayerHasUnit($this);
        $this->armyHasUnit = $armyHasUnit;

        return $this;
    }

    /**
     * Get ArmyHasUnit entity (one to one).
     *
     * @return \DataBundle\Entity\ArmyHasUnit
     */
    public function getArmyHasUnit()
    {
        return $this->armyHasUnit;
    }

    /**
     * Set VillageHasUnit entity (one to one).
     *
     * @param \DataBundle\Entity\VillageHasUnit $villageHasUnit
     * @return \DataBundle\Entity\PlayerHasUnit
     */
    public function setVillageHasUnit(VillageHasUnit $villageHasUnit = null)
    {
        $villageHasUnit->setPlayerHasUnit($this);
        $this->villageHasUnit = $villageHasUnit;

        return $this;
    }

    /**
     * Get VillageHasUnit entity (one to one).
     *
     * @return \DataBundle\Entity\VillageHasUnit
     */
    public function getVillageHasUnit()
    {
        return $this->villageHasUnit;
    }

    /**
     * Set Player entity (many to one).
     *
     * @param \DataBundle\Entity\Player $player
     * @return \DataBundle\Entity\PlayerHasUnit
     */
    public function setPlayer(Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get Player entity (many to one).
     *
     * @return \DataBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set Constructible entity (many to one).
     *
     * @param \DataBundle\Entity\Constructible $constructible
     * @return \DataBundle\Entity\PlayerHasUnit
     */
    public function setConstructible(Constructible $constructible = null)
    {
        $this->constructible = $constructible;

        return $this;
    }

    /**
     * Get Constructible entity (many to one).
     *
     * @return \DataBundle\Entity\Constructible
     */
    public function getConstructible()
    {
        return $this->constructible;
    }

    public function __sleep()
    {
        return array('id', 'player_id', 'constructible_id');
    }
}